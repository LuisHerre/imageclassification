from flask import Flask, jsonify, request
from flask_restful import Api, Resource
from pymongo import MongoClient
from passlib.hash import sha256_crypt
import requests
import subprocess
import json

app = Flask(__name__)
api = Api(app)

client = MongoClient("mongodb://db:27017")
db = client.ImageRecognition
users = db["Users"]

def UserExist(username):
    if users.find({"Username": username}).count() == 0:
        return False
    else:
        return True

class Register(Resource):
    def post(self):
        postedData = request.get_json()

        username = postedData["username"]
        password = postedData["password"]

        if UserExist(username):
            retJSON = {
                "status": 301,
                "msg": "Invalid Username"
            }
            return jsonify(retJSON)

        hashed_pw = sha256_crypt.encrypt(password)

        users.insert({
            "Username": username,
            "Password": hashed_pw,
            "Tokens": 6
        })

        retJSON = {
            "status": 200,
            "msg": "Successfully signed up for the API"
        }
        return jsonify(retJSON)

def generateReturnDictionary(status, msg):
    retJSON = {
        "status": status,
        "msg": msg
    }
    return retJSON

def verify_pw(username, password):
    hashed_pw = users.find({
        "Username": username
    })[0]["Password"]

    return (sha256_crypt.verify(password, hashed_pw))


def verifyCredentials(username, password):
    if not UserExist(username):
        return generateReturnDictionary(301, "Invalid Password"), True
    
    correct_pw = verify_pw(username, password)
    if not correct_pw:
        return generateReturnDictionary(302, "Invalid Password"), True

    return None, False

class Classify(Resource):
    def post(self):
        postedData = request.get_json()

        username = postedData["username"]
        password = postedData["password"]
        url       = postedData["url"]

        # do
        retJSON, error = verifyCredentials(username, password)
        if error:
            return jsonify(retJSON)

        tokens = users.find({
            "Username": username
        })[0]["Tokens"]

        # do
        if tokens <= 0:
            return jsonify( generateReturnDictionary(303, "Not Enough Tokens!") )
        
        r = requests.get(url)
        retJSON = {}
        with open("temp.jpg","wb") as f:
            f.write(r.content)

        proc = subprocess.Popen('python classify_image.py --model_dir=. --image_file=./temp.jpg', shell=True)
        proc.communicate()[0]
        proc.wait()
        
        with open("text.txt") as g:
            retJSON = json.load(g)

        users.update({
            "Username": username
        },{
            "$set": {
                "Tokens": tokens-1
            }
        })
        return retJSON

class Refill(Resource):
    def post(self):
        postedData = request.get_json()

        username = postedData["username"]
        password = postedData["admin_pw"]
        refill_amount = postedData["refill"]

        if not UserExist(username):
            retJSON = {
                "status": 301,
                "msg": "Invalid Username"
            }
            return jsonify(retJSON)

        # fix this soon
        correct_pw = "abc123"
        if not password == correct_pw:
            retJSON = {
                "status": 304,
                "msg": "Invalid Admin Password"
            }
            return jsonify(retJSON)

        users.update({
            "Username": username
        },{
            "$set":{
                "Tokens": refill_amount
            }
        })

        retJSON = {
            "status": 200,
            "msg": "Refilled Successfully"
        }
        return jsonify(retJSON)

api.add_resource(Register, '/register')
api.add_resource(Classify, '/classify')
api.add_resource(Refill, '/refill')

if __name__ == "__main__":
    app.run(host='0.0.0.0')